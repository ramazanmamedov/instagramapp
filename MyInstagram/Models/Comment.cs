﻿using System;

namespace MyInstagram.Models
{
    public class Comment
    {
        public string Id { get; set; }
        public Post Publication { get; set; }
        public string PublicationId { get; set; }
        public string CommentMessage { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
