﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace MyInstagram.Models
{
    public class ApplicationContext : IdentityDbContext<User>
    {
        public new DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Followers> Followers { get; set; }

        public ApplicationContext(DbContextOptions options) : base(options)
        {
        }
    }
}
