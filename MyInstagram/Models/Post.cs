﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyInstagram.Models
{
    public class Post
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string PublicationImagePath { get; set; }
        public User User { get; set; }
        public string UserName { get; set; }
        public string UserId { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
