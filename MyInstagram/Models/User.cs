﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
using MyInstagram.Enums;

namespace MyInstagram.Models
{
    public class User : IdentityUser
    {
        [Required]
        public string AvatarImagePath { get; set; }
        public string Name { get; set; }
        public string AboutMe { get; set; }
        public GenderType Gender { get; set; }
    }
}
