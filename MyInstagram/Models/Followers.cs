﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyInstagram.Models
{
    public class Followers
    {
        public string Id { get; set; }
        public string FollowerUserId { get; set; }
        public string FollowingUserId { get; set; }
    }
}
