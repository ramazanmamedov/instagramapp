﻿using System;

namespace MyInstagram.Models
{
    public class Like
    {
        public string Id { get; set; }
        public Post Publication { get; set; }
        public string PublicationId { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
    }
}