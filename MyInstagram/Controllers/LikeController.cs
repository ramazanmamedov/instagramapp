﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyInstagram.Models;
using MyInstagram.ViewModels;

namespace MyInstagram.Controllers
{
    public class LikeController : Controller
    {
        private readonly ApplicationContext _context;
        private readonly UserManager<User> _userManager;

        public LikeController(ApplicationContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddLikeViewModel model)
        {
            User user = await _userManager.FindByNameAsync(User.Identity.Name);
            Like like = new Like
            {
                UserId = model.UserId,
                PublicationId = model.PublicationId
            };
            if (_context.Likes.Any(l => l.UserId == user.Id && l.PublicationId == model.PublicationId))
            {
                return RedirectToAction("Delete", like);
            }

            if (ModelState.IsValid)
            {
               await _context.Likes.AddAsync(like);
               await _context.SaveChangesAsync();
            }

            return RedirectToAction("Counter", new {postId = like.PublicationId});
        }

        public async Task<IActionResult> Delete(Like like)
        {
            var result = await _context.Likes.
                FirstAsync(l => l.UserId == like.UserId && l.PublicationId == like.PublicationId);
            if (result != null)
            {
                _context.Likes.Remove(result);
                await _context.SaveChangesAsync();
            }
            
            return RedirectToAction("Counter", new { postId = like.PublicationId });
        }

        public IActionResult Counter(string postid)
        {
            var count = _context.Likes.Where(l => l.PublicationId == postid).ToList();
            return Json(count.Count);
        }
    }
}