﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using MyInstagram.Models;
using MyInstagram.ViewModels;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace MyInstagram.Controllers
{
    public class UserController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ApplicationContext _context;

        public UserController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        public async Task<IActionResult> Index()
        { 
            User user = await _userManager.FindByNameAsync(User.Identity.Name);
            List<Post> posts = _context.Posts.Where(p => p.UserName == user.UserName).ToList();
            UserViewModel model = new UserViewModel
            {
                Id = user.Id,
                UserName = user.UserName,
                Name = user.Name,
                AboutMe = user.AboutMe,
                AvatarImagePath = user.AvatarImagePath,
                Posts = posts
            };   
            
            model.Following = _context.Followers.Where(f => f.FollowingUserId == model.Id).ToList();
            model.Followers = _context.Followers.Where(f => f.FollowerUserId == model.Id).ToList();
            return View(model);
          
        }

        [HttpGet]
        public IActionResult Register() => View();
        
        [HttpPost]
        public async Task<IActionResult> Register(RegisterUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (var stream = new FileStream(model.GetFilePath(), FileMode.Create))
                {
                    model.Avatar.CopyTo(stream);
                }

                User user = model.GetUser();
                user.AvatarImagePath = model.GetAvatarImagePath();
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return RedirectToAction("Register", "User");
        }

        [HttpGet]
        public IActionResult LogIn(string returnUrl = null)
        {
            return View(new LogInViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        public async Task<IActionResult> LogIn(LogInViewModel model)
        {
            SignInResult result = SignInResult.Failed;
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByNameAsync(model.UserName);
                if (user != null)
                {
                    result = await _signInManager.PasswordSignInAsync(user,
                        model.Password, model.RememberMe, false);
                }

                if (result.Succeeded)
                {
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                }
            }
            return View(model);
        }
    

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("LogIn", "User");
        }

        public IActionResult CheckName(RegisterUserViewModel model)
        {
            var user = model.GetUser();
            model.UserName = model.UserName.Trim();
            bool isTrue;
            if (string.IsNullOrEmpty(user.Id))
                isTrue = _userManager.Users.Any(b => string.Equals(b.UserName.Trim(), model.UserName,
                    StringComparison.CurrentCultureIgnoreCase));
            else
            {
                if (_userManager.Users.Any(b => string.Equals(b.UserName.Trim(), model.UserName,
                                                   StringComparison.CurrentCultureIgnoreCase) && b.Id == user.Id))
                {
                    return Ok(true);
                }
                if (_userManager.Users.Any(b => string.Equals(b.UserName.Trim(), model.UserName,
                        StringComparison.CurrentCultureIgnoreCase)))
                {
                    return Ok(false);
                }
                return Ok(true);
            }
            return Ok(isTrue);
        }

        public IActionResult CheckEmail(RegisterUserViewModel model)
        {
            var user = model.GetUser();
            model.Email = model.Email.Trim();
            bool isTrue;
            if (string.IsNullOrEmpty(user.Id))
                isTrue = _userManager.Users.Any(b => string.Equals(b.Email.Trim(), model.Email,
                    StringComparison.CurrentCultureIgnoreCase));
            else
            {
                if (_userManager.Users.Any(b => string.Equals(b.Email.Trim(), model.Email,
                                                    StringComparison.CurrentCultureIgnoreCase) && b.Id == user.Id))
                {
                    return Ok(true);
                }
                if (_userManager.Users.Any(b => string.Equals(b.Email.Trim(), model.Email,
                    StringComparison.CurrentCultureIgnoreCase)))
                {
                    return Ok(false);
                }
                return Ok(true);
            }
            return Ok(isTrue);
        }

        public async Task<IActionResult> Details(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (string.IsNullOrEmpty(userId))
            {
                RedirectToAction("Details");
            }
            if (User.Identity.IsAuthenticated)
            {
                User currentUser = await _userManager.FindByNameAsync(User.Identity.Name);
                if (user.Id == currentUser.Id)
                {
                    return RedirectToAction("Index");
                }
            }

            UserViewModel model = new UserViewModel
            {
                Id = user.Id,
                AboutMe = user.AboutMe,
                AvatarImagePath = user.AvatarImagePath,
                Name = user.Name,
                UserName = user.UserName,
                Posts = _context.Posts.Where(p=> p.UserId == user.Id).ToList()
            };
            model.Following = _context.Followers.Where(f => f.FollowingUserId == model.Id).ToList();
            model.Followers = _context.Followers.Where(f => f.FollowerUserId == model.Id).ToList();
;           return View(model);
        }

        public async Task<IActionResult> Follow(string followingId, string followerId)
        {
            Followers followers = new Followers
            {
                FollowerUserId = followerId,
                FollowingUserId = followingId
            };
            if (_context.Followers.Any(f => f.FollowerUserId == followerId && f.FollowingUserId == followingId))
            {
                var result =  _context.Followers.First(f =>
                    f.FollowerUserId == followerId && f.FollowingUserId == followingId);
                _context.Followers.Remove(result);
                await _context.SaveChangesAsync();
            }
            else
            {
                await _context.AddAsync(followers);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction("Counter", "User", new{followerId});
        }
        
        public IActionResult Counter(string followerId)
        {
            var count = _context.Followers.Where(f => f.FollowerUserId == followerId).ToList();
            return Json(count.Count);
        }
        
        public IActionResult SearchAjaxResult(string keyWord)
        {
            if (!string.IsNullOrEmpty(keyWord))
            {
                keyWord = keyWord.ToLower().Trim();
            }

            var users = _userManager.Users.Where(u => u.UserName.ToLower().Contains(keyWord)
                                                      || u.Email.ToLower().Contains(keyWord)
                                                      || u.Name.ToLower().Contains(keyWord)
                                                      || u.AboutMe.ToLower().Contains(keyWord)).ToList();
            return PartialView("PartialViews/UserSearchResult", users);
        }
    }
}