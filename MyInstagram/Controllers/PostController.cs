﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyInstagram.Models;
using MyInstagram.ViewModels;

namespace MyInstagram.Controllers
{
    public class PostController : Controller
    {
        private readonly ApplicationContext _context;
        private readonly UserManager<User> _userManager;

        public PostController(ApplicationContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult AddPost() => View();
        [HttpPost]
        public async Task<IActionResult> Add(AddPostViewModel model)
        {
            Post post = new Post
            {
                UserId = _userManager.GetUserId(User),
                Description = model.Description,
                UserName = User.Identity.Name,
                PublicationImagePath = model.GetImagePath()
            };
            using (var stream = new FileStream(model.GetFilePath(), FileMode.Create))
            {
               await model.Image.CopyToAsync(stream);
            }

            await _context.Posts.AddAsync(post);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "User");
        }

        public async Task<IActionResult> Delete(string postId)
        {
            Post post = _context.Posts.Find(postId);
            List<Like> likes = _context.Likes.Where(l => l.PublicationId == post.Id).ToList();
            List<Comment> comments = _context.Comments.Where(c => c.PublicationId == post.Id).ToList();
            if (post != null)
            {
                _context.Likes.RemoveRange(likes);
                _context.Comments.RemoveRange(comments);
                _context.Posts.Remove(post);
                await _context.SaveChangesAsync();
            }
            return Json(_context.Posts.Count(p => p.UserId == _userManager.GetUserId(User)));
        }

        public async Task<IActionResult> EditDescription(string postId, string content)
        {
            Post post = await _context.Posts.FindAsync(postId);
            post.Description = content;
            post.Id = postId;
            _context.Entry(post).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return Json(post.Description);
        }
    }
}