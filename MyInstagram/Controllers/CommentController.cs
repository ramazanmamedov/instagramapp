﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyInstagram.Models;
using MyInstagram.ViewModels;

namespace MyInstagram.Controllers
{
    public class CommentController : Controller
    {
        private ApplicationContext _context;

        public CommentController(ApplicationContext context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<IActionResult> Add(string publicationId, string userId,  string commentMessage)
        {
            Comment comment = new Comment
            {
                CommentMessage = commentMessage,
                Date = DateTime.Now,
                PublicationId = publicationId,
                UserId = userId,
                UserName = User.Identity.Name
            };
            await _context.Comments.AddAsync(comment);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}