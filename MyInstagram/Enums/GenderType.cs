﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyInstagram.Enums
{
    public enum GenderType
    {
        Male = 1,
        Female = 2
    }
}
