﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyInstagram.Models;

namespace MyInstagram.ViewModels
{
    public class PostViewModel
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string PublicationImagePath { get; set; }
        public List<Like> Likes { get; set; }
    }
}
