﻿using System.ComponentModel.DataAnnotations;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyInstagram.Enums;
using MyInstagram.Models;


namespace MyInstagram.ViewModels
{
    public class RegisterUserViewModel
    {
        [Required]
        [Remote("CheckName", "User", ErrorMessage = "This User Name is busy", AdditionalFields = "Id")]
        public string UserName { get; set; }
        [Required]
        [Remote("CheckEmail", "User", ErrorMessage = "This Email is busy", AdditionalFields = "Id")]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public IFormFile Avatar { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        public string Name { get; set; }
        [StringLength(100)]
        public string AboutMe { get; set; }
        public string PhoneNumber { get; set; }
        public GenderType Gender { get; set; }

        public string GetFilePath()
        {
            return Path.Combine(
                Directory.GetCurrentDirectory(),
                "wwwroot",
                "images",
                Avatar.FileName
            );
        }

        public string GetAvatarImagePath()
        {
            return Path.Combine(
                "images",
                Avatar.FileName
            );
        }

        public User GetUser()
        {
            User user = new User
            {
                Name = Name,
                Email = Email,
                UserName = UserName,
                Gender = Gender,
                AboutMe = AboutMe,
                PhoneNumber = PhoneNumber
            };

            return user;
        }
    }
}
