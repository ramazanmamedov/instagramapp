﻿namespace MyInstagram.ViewModels
{
    public class AddLikeViewModel
    {
        public string Id { get; set; }
        public string PublicationId { get; set; }
        public string UserId { get; set; }

        public AddLikeViewModel(string publicationId)
        {
            PublicationId = publicationId;
        }

        public AddLikeViewModel()
        {
            
        }

    }
}