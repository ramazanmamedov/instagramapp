﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyInstagram.ViewModels
{
    public class CommentViewModel
    {
        public string Id { get; set; }
        public string PublicationId { get; set; }
        public string CommentMessage { get; set; }
        public string UserId { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
        public string UserName { get; set; }

        public CommentViewModel(string publicationId)
        {
            PublicationId = publicationId;
        }

        public CommentViewModel()
        {
            
        }
    }
}
