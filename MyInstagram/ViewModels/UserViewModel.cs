﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MyInstagram.Enums;
using MyInstagram.Models;

namespace MyInstagram.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string AvatarImagePath { get; set; }
        public string Name { get; set; }
        public string AboutMe { get; set; }
        public List<Post> Posts { get; set; }
        public List<Followers> Followers { get; set; }
        public List<Followers> Following { get; set; }
    }
}
