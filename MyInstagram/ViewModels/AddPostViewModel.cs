﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace MyInstagram.ViewModels
{
    public class AddPostViewModel
    {
        public string Id { get; set; }
        [Required]
        public IFormFile Image { get; set; }
        public string Description { get; set; }
        public string UserName { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;

        public string GetFilePath()
        {
            return Path.Combine(
                Directory.GetCurrentDirectory(),
                "wwwroot",
                "publicationImages",
                Image.FileName
            );
        }

        public string GetImagePath()
        {
            return Path.Combine(
                "publicationImages",
                Image.FileName
            );
        }
    }
}
